package com.example.currentweatherdatabinding

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.ImageView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.example.currentweatherdatabinding.databinding.ActivityMainBinding
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.io.IOException
import java.io.InputStream
import java.net.URL
import java.util.*
import com.google.gson.annotations.SerializedName

class WeatherLayout(){
    companion object{
        val weatherIcons = mapOf(
            Pair("01d", R.drawable.i01d),
            Pair("02d", R.drawable.i02d),
            Pair("03d", R.drawable.i03d),
            Pair("04d", R.drawable.i04d),
            Pair("09d", R.drawable.i09d),
            Pair("10d", R.drawable.i10d),
            Pair("11d", R.drawable.i11d),
            Pair("13d", R.drawable.i13d),
            Pair("50d", R.drawable.i50d),
            Pair("01n", R.drawable.i01n),
            Pair("02n", R.drawable.i02n),
            Pair("03n", R.drawable.i03n),
            Pair("04n", R.drawable.i04n),
            Pair("09n", R.drawable.i09n),
            Pair("10n", R.drawable.i10n),
            Pair("11n", R.drawable.i11n),
            Pair("13n", R.drawable.i13n),
            Pair("50n", R.drawable.i50n),
            Pair("none", R.drawable.none)
        )
        val windIcons = mapOf(
            Pair(30, R.drawable.w030),
            Pair(45, R.drawable.w045),
            Pair(60, R.drawable.w060),
            Pair(90, R.drawable.w090),
            Pair(120, R.drawable.w120),
            Pair(135, R.drawable.w135),
            Pair(150, R.drawable.w150),
            Pair(180, R.drawable.w180),
            Pair(210, R.drawable.w210),
            Pair(240, R.drawable.w240),
            Pair(270, R.drawable.w270),
            Pair(300, R.drawable.w300),
            Pair(315, R.drawable.w315),
            Pair(330, R.drawable.w330),
            Pair(360, R.drawable.w360),
            Pair(117, R.drawable.none)
        )
    }
    var dayLengthMinutes: String = ""
    var dayLengthHours: String = ""
    var dayLengthStr: String = ""
    var dayLength: Int = 0
        set(smtVal){
            dayLengthMinutes = (smtVal/10/60).toString()
            dayLengthHours = (smtVal/60/60).toString()
            dayLengthStr = "Продолжительность дня: "+dayLengthHours+":"+dayLengthMinutes
            field = smtVal
        }
    var windIconID: Int = windIcons[30]!!
    var windDegrees: Int = 0
        set(smtVal){
            field = smtVal
            if (smtVal == -1) {
                windIconID = windIcons[117]!!
            } else {
                val keys: List<Int> = windIcons.keys.toList()
                for (i in 1 until keys.size){
                    if ((smtVal <= keys[i]) && (smtVal >= keys[i-1])) {
                        windIconID = windIcons[keys[i]]!!
                    }
                }
            }
            Log.d("windIconID", windIconID.toString())
        }

    var weatherIconID: Int = 0
    var weatherIcon: String = ""
        set(smtVal){
            field = smtVal
            weatherIconID = weatherIcons[smtVal]!!
            Log.d("weatherIconID", weatherIconID.toString())
        }
    var degStr: String = ""
    var speedStr: String = ""
    var tempStr: String = ""
}

data class WeatherAll(
    @SerializedName("weather") var weather : ArrayList<Weather> = arrayListOf(),
    @SerializedName("main") var main : Main? = Main(),
    @SerializedName("wind") var wind : Wind? = Wind(),
    @SerializedName("sys") var sys : Sys? = Sys()
)
data class Weather(
    @SerializedName("icon") var icon : String? = null
)
data class Main(
    @SerializedName("temp") var temp: Double? = null
)
data class Wind(
    @SerializedName("speed") var speed : Double? = null,
    @SerializedName("deg") var deg   : Int?    = null
)
data class Sys(
    @SerializedName("sunrise") var sunrise : Int? = null,
    @SerializedName("sunset") var sunset : Int? = null
)


class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    private lateinit var cityName: EditText
    private lateinit var deg: TextView
    private lateinit var speed: TextView
    private lateinit var temp: TextView
    private lateinit var dayLength: TextView
    private lateinit var weatherIconView: ImageView
    private lateinit var windIconView: ImageView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        binding  = DataBindingUtil.setContentView(this, R.layout.activity_main)
        cityName = findViewById(R.id.cityName)
        deg = findViewById(R.id.deg)
        speed = findViewById(R.id.speed)
        temp = findViewById(R.id.temp)
        dayLength = findViewById(R.id.dayLength)
        weatherIconView = findViewById(R.id.weatherIcon)
        windIconView = findViewById(R.id.windIcon)
    }
    suspend fun loadWeather() {
        val API_KEY = resources.getString(R.string.API_KEY)
        val city = cityName.text.toString()
        val weatherURL = "https://api.openweathermap.org/data/2.5/weather?q="+"$city"+"&appid="+"$API_KEY"+"&units=metric"
        try {
            val stream = URL(weatherURL).getContent() as InputStream

            val data = Scanner(stream).nextLine()

            val gson = Gson()
            val weatherAll: WeatherAll = gson.fromJson<WeatherAll>(data, WeatherAll::class.java)
            Log.d("mytag2", "test")

            val weatherLayout = WeatherLayout()
            weatherLayout.degStr = "Угол ветра: " + weatherAll.wind?.deg!!.toString()
            weatherLayout.speedStr = "Скорость ветра: " + weatherAll.wind?.speed!!.toString()
            weatherLayout.tempStr = "Температура: " + weatherAll.main?.temp!!.toString()
            weatherLayout.dayLength = weatherAll.sys?.sunset!! - weatherAll.sys?.sunrise!!
            weatherLayout.windDegrees = weatherAll.wind?.deg!!
            weatherLayout.weatherIcon = weatherAll.weather[0].icon!!
            Log.d("mytag4", "test")

            binding.weatherLayout = weatherLayout
        }
        catch (e: java.lang.Exception){
            val weatherLayout = WeatherLayout()
            weatherLayout.degStr = "An error has occurred"
            weatherLayout.speedStr = ""
            weatherLayout.tempStr = ""
            weatherLayout.dayLength = 0
            weatherLayout.windDegrees = -1
            weatherLayout.weatherIcon = "none"
            binding.weatherLayout = weatherLayout
        }
    }
    public fun onClick(v: View) {

        GlobalScope.launch (Dispatchers.IO) {
            loadWeather()
        }
    }
}
