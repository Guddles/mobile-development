import kotlin.math.pow
import kotlin.math.PI
import kotlin.math.sqrt

interface FigureInterface {
    val figureName: String

    fun printSidesCount()
    fun printSides()
}
interface Transforming{
    fun resize(mul: Int)
    fun rotate(dir: RotateDir, centerX: Int, centerY: Int)
}
interface Movable{
    fun move(dx:Int, dy:Int)
}
enum class RotateDir{
    CLOCK_WISE, COUNTER_CLOCK_WISE
}
abstract class Figure : FigureInterface {
    protected lateinit var sides: FloatArray

    abstract fun getArea() : Float

    override fun printSidesCount() {
        if (!this::sides.isInitialized) return
        println("Count sides of ${figureName}: ${sides.size}")
    }

    override fun printSides() {
        if (!this::sides.isInitialized) return
        print("Sides of ${figureName}:")
        sides.forEach { print(it); print(' ') }
        println()
    }

}


class Rectangle(var x: Int, var y :Int,a: Float, b: Float = -1F) : Figure(), Transforming, Movable {
    init {
        sides = FloatArray(2)
        sides[0] = a
        sides[1] = if (b <= 0) a else b
    }
    // Hotkey: CTRL + O
    override fun getArea(): Float {
        return sides[0] * sides[1]
    }

    override val figureName: String
        get() = if (sides[0] == sides[1]) "Square" else "Rectangle"

    override fun resize(mul: Int) {
        x *= mul
        y *= mul
    }

    override fun rotate(dir: RotateDir, centerX: Int, centerY: Int) {
        if (centerX == x && centerY == y) return
        val (signX, signY) = if (dir == RotateDir.CLOCK_WISE) Pair(1,-1) else Pair(-1,1)
        x = (signX * (y-centerY)+centerX).also {
            y = (signY * (x-centerX)+centerX)
        }
        sides[0] = sides[1].also{
            sides[1] = sides[0]
        }

    }
    override fun move(dx: Int, dy: Int) {
        x +=dx
        y += dy
    }

    override fun toString(): String {
        return ("Прямоугольник ($x, $y)[width: ${sides[1]}, height: ${sides[0]}]")
    }
}
class Square(var x: Int, var y: Int, var width: Int) : Transforming, Figure(), Movable {
    override fun getArea(): Float {
        return width.toFloat() * width.toFloat()
    }

    override val figureName: String
        get() = "Square"

    override fun resize(mul: Int) {
        width *= mul
    }

    override fun rotate(dir: RotateDir, centerX: Int, centerY: Int) {
        if (centerX == x && centerY == y) return
        val (signX, signY) = if (dir == RotateDir.COUNTER_CLOCK_WISE) Pair(-1, 1) else Pair(1, -1)

        x = (signX * (y - centerY) + centerX).also {
            y = signY * (x - centerX) + centerY
        }
    }

    override fun move(dx: Int, dy: Int) {
        x += dx
        y += dy
    }

    override fun toString(): String {
        return "Квадрат ($x, $y) [w: $width]"
    }
}


class Triangle(a: Float, b: Float, c: Float) : Figure() {

    init {
        sides = FloatArray(3)
        sides[0] = a
        sides[1] = b
        sides[2] = c
    }

    override fun getArea(): Float {
        val p = (sides[0] + sides[1] + sides[2]) / 2
        return sqrt(p * (p - sides[0]) * (p - sides[1]) * (p - sides[2]))
    }

    override val figureName: String
        get() = "Triangle"
}

class Circle(private var r: Float, var x:Int, var y: Int) : Figure(), Transforming, Movable {
    override fun getArea(): Float {
        return Math.PI.toFloat() * r * r
    }

    override fun printSidesCount() {
        println(0)
    }

    override fun printSides() {
        println("Круг не имеет сторон")
    }

    override val figureName: String
        get() = "Circle"

    override fun move(dx: Int, dy: Int) {
        x += dx
        y += dy
    }

    override fun resize(mul: Int) {
        r *= mul
    }

    override fun rotate(dir: RotateDir, centerX: Int, centerY: Int) {
        if (centerX == x && centerY == y) return
        val (signX, signY) = if (dir == RotateDir.COUNTER_CLOCK_WISE) Pair(-1, 1) else Pair(1, -1)

        x = (signX * (y - centerY) + centerX).also {
            y = signY * (x - centerX) + centerY
        }
    }

    override fun toString(): String {
        return "Круг ($x, $y) [radius: $r]"
    }
}


fun main() {
//    val square = Rectangle(2F)
//    println(square.getArea())
//    square.printSidesCount()
//    square.printSides()

    val rect = Rectangle(0,3,3F, 5F)
    println("Прямоугольник до перемещения  ${rect.toString()}")
    rect.move(1,3)
    println("Прямоугольник после перемещения  ${rect.toString()}")

    println("Прямоугольник до resize  ${rect.toString()}")
    rect.resize(5)
    println("Прямоугольник после resize  ${rect.toString()}")
    println("=========================================================")

    val triangle = Triangle(4.5F, 5F, 3.65F)






//    println(triangle.getArea())
//    triangle.printSidesCount()
//    triangle.printSides()

    val circle = Circle(5F, 0,1)

    println("Круг до перемещения  ${circle.toString()}")
    circle.move(1,3)
    println("Круг после перемещения  ${circle.toString()}")

    println("Круг до resize  ${circle.toString()}")
    circle.resize(7)
    println("Круг после resize  ${circle.toString()}")

    println("=========================================================")
    val sqr = Square(1,5,10)
    println("Квадрат до перемещения  ${sqr.toString()}")
    sqr.move(1,3)
    println("Квадрат после перемещения  ${sqr.toString()}")
    println("=========================================================")
    println("Квадрат до resize  ${sqr.toString()}")
    sqr.resize(5)
    println("Квадрат после resize  ${sqr.toString()}")
    println("Квадрат до поворта  ${sqr.toString()}")
    sqr.rotate(RotateDir.COUNTER_CLOCK_WISE, -1,1)
    println("Квадрат после поворота  ${sqr.toString()}")





//    println(circle.getArea())
//    circle.printSidesCount()
//    circle.printSides()
}