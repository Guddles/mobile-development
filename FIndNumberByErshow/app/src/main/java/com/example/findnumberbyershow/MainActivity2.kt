package com.example.findnumberbyershow

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import kotlin.math.min

class MainActivity2 : AppCompatActivity() {
    var minNum = 0
    var maxNum = 0
    private lateinit var textView: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        minNum = intent.getIntExtra("min", 0)
        maxNum = intent.getIntExtra("max", 0)
        textView = findViewById(R.id.textView)
        textView.text = ((maxNum + minNum)/2).toString()
    }
    fun guess(view: View){
        if (maxNum - minNum <=1){
            textView.text = "Вы загадали $minNum"
        }
        else{
            if(view.id == R.id.lower){
                maxNum = (maxNum+minNum)/2
            }
            else if(view.id == R.id.bigger){
                minNum = (maxNum+ minNum)/2
            }
            textView.text = "${((maxNum + minNum) / 2)}"
        }


    }
    fun again(view: View){
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
}