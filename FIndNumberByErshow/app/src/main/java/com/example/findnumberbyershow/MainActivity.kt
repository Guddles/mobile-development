package com.example.findnumberbyershow

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    private lateinit var minValue: EditText
    private lateinit var maxValue: EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        minValue = findViewById(R.id.minId)
        maxValue = findViewById(R.id.maxId)
    }

    fun startClick(view: View){
        val minNum =minValue.text.toString().toInt()
        val maxNum = maxValue.text.toString().toInt()

        if (minNum >= maxNum){
            Toast.makeText(this, "Минимум должен быть мньше Максимума", Toast.LENGTH_LONG).show()
        }
        else{
            val intent = Intent(this, MainActivity2::class.java)
            intent.putExtra("min", minNum)
            intent.putExtra("max", maxNum)
            startActivity(intent)
        }

    }
}