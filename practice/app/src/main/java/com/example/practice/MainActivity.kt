package com.example.practice

import android.os.Bundle
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.practice.databinding.ActivityMainBinding
import android.graphics.Color
import android.util.Log
import com.example.practice.network.ApiClient

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val entrantTable = findViewById<TableLayout>(R.id.table_layout)
        val table_row1 = findViewById<TableRow>(R.id.myrow)
        val authToken =
            "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI1ZDM4NmMwMC1hNGQzLTllZjQtYzNjNy02NTE1Y2M3NTNjZmIiLCJqdGkiOiJkNTIyMjNhYjczM2YwMjk0OTc1MjI3YTdhZjhmNDI0MzZiMmE4MTE3ODA3NGUyZmM2ODQ4ZGIyMzg3ZjdlNGFhMDU0ODUwZGQ2ZmQzN2RmZSIsImlhdCI6MTY5Njk2ODYxMy40NDIwNTYsIm5iZiI6MTY5Njk2ODYxMy40NDIwNTksImV4cCI6MTY5Njk3MjIxMy40MDQ0OTEsInN1YiI6IjEiLCJzY29wZXMiOltdfQ.bJ_flWd4xrKgmYqswu50kIYpQAdQP8vDAmcfzb8PTAqQZCKoc3oEBHZ4H607VTV5QCk1-F6WvSDVMNwippDg6yiGChBQ9drcgdEb_fP_mc1seTu3naVr-mLVmpftO3CDBVkebZhn1nwGpyYPSpON46bgBAd5hb0VzK1e0TK8xxu49graTthHkHVff5Gy6VFZKfwzsPA8gLqxVPTLnYBV5vNDpf2EvEZcSSA8pQu00JXDkahlm9RkJgw6cCxpPe5qpq4X9IgIkDBsk_SRdkfRVaNHKOMFqwR9OumbFp_vqqNG09Zyug-2BLhhBR1K4dzO2O7i_cHhvE789oJZAUQFpg"

        CoroutineScope(Dispatchers.IO).launch {
            try {
                val response = ApiClient.entrantReq.getEntrants(authToken)
                withContext(Dispatchers.Main) {
                    if (response.isSuccessful) {
                        // Вставка ответа в соответствии с data class в поля таблицы
                        response.body()?.let{
                            entrantResponse -> for(entrant in entrantResponse.data){
                                Log.d("Resp", "$entrant")
                                entrantTable.addView(createTableRow(entrant))
                        }
                        }
                    } else {
                        Log.e("Error", "Response is not a list")
                    }
                }
            } catch (e: Exception) {
                Log.e("Error", e.message.toString())
            }


//        val call = EntrantApiClient.getEntrans(authToken)
//        call.enqueue(object : Callback<List<Entrant>> {
//            override fun onResponse(call: Call<List<Entrant>>, response: Response<List<Entrant>>) {
//                if (response.isSuccessful) {
//                    val gson = Gson()
//                    val json = gson.toJson(response.body())
//                    val entrants = gson.fromJson<List<Entrant>>(json, object : TypeToken<List<Entrant>>() {}.type)
//                    Log.i("EntrantApiClientResponse", "Response: $entrants")
//                    // Обработайте список объектов типа Entrant
//                } else {
//                    Log.e("EntrantApiClientErr", "Error: ${response.code()}")
//                    // Обработайте ошибку
//                }
//            }
//
//            override fun onFailure(call: Call<List<Entrant>>, t: Throwable) {
//                Log.e("EntrantApiClientFail", "Error: ${t.message}")
//                // Обработайте ошибку
//            }
//        })

//        val entrants = listOf(
//            Entrant("John Doe", "555-1234", "РПО", "1", "90", "123-456-789 01", "Accepted"),
//            Entrant("Jane Smith", "555-5678", "Дизайн", "2", "85", "987-654-321 02", "Rejected"),
//            Entrant("Jandsfe Smith", "555-5678", "Дизайн", "2", "85", "987-654-321 02", "Rejected"),
//            Entrant("Bob Johnson", "555-9012", "РПО", "3", "80", "456-789-123 03", "Accepted")
//        )

//        for (entrant in entrants) {
//            val row = TableRow(this)
//            val nameView = TextView(this)
//            nameView.text = entrant.name
//            row.addView(nameView)
//
//
//
//            val phoneView = TextView(this)
//            phoneView.text = entrant.phone
//            row.addView(phoneView)
//
//            val specialityView = TextView(this)
//            specialityView.text = entrant.speciality
//            row.addView(specialityView)
//
//            val priorityView = TextView(this)
//            priorityView.text = entrant.priority
//            row.addView(priorityView)
//
//            val scoreView = TextView(this)
//            scoreView.text = entrant.score
//            row.addView(scoreView)
//
//            val snilsView = TextView(this)
//            snilsView.text = entrant.snils
//            row.addView(snilsView)
//
//            val commentView = TextView(this)
//            commentView.text = entrant.comment
//            row.addView(commentView)
//
//            entrantTable.addView(row)
//
//            val red = Random.nextInt(256)
//            val green = Random.nextInt(256)
//            val blue = Random.nextInt(256)
//            val color = Color.rgb(red, green, blue)
//            row.setBackgroundColor(color)

        }
    }

        fun createTableRow(entrant: Entrant): TableRow {
            val row = TableRow(this)
            val nameTextView = TextView(this)
            nameTextView.text = entrant.attributes.name
            row.addView(nameTextView)
            val phoneTextView = TextView(this)
            phoneTextView.text = entrant.attributes.phone
            row.addView(phoneTextView)
            val specialityTextView = TextView(this)
            specialityTextView.text = entrant.attributes.specialty
            row.addView(specialityTextView)
            val priorityTextView = TextView(this)
            priorityTextView.text = entrant.attributes.priority
            row.addView(priorityTextView)
            val scoreTextView = TextView(this)
            scoreTextView.text = entrant.attributes.score
            row.addView(scoreTextView)
            val snilsTextView = TextView(this)
            snilsTextView.text = entrant.attributes.snils
            row.addView(snilsTextView)
            val commentTextView = TextView(this)
            commentTextView.text = entrant.attributes.comment
            row.addView(commentTextView)

            return row
        }

}



//        val entrants = listOf(
//            Entrant("Иванов \nИван \nИванович", "+7 (123) 456-78-90", "Разработка программного обеспечения", "1", "100", "123-456-789 01", "счмсчмсчм"),
//            Entrant("Петров Петр Петрович", "+7 (987) 654-32-10", "Дизайн", "3", "80", "987-654-321 09", ""),
//            Entrant("Сидоров Сидор Сидорович", "+7 (555) 555-55-55", "Дизайн", "2", "60", "555-555-555 55", "")
//        )
//
//        val recyclerView: RecyclerView = findViewById(R.id.recyclerView)
//        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
//        recyclerView.adapter = EntrantAdapter(entrants)


