package com.example.practice

import com.google.gson.annotations.SerializedName

//data class Entrant (
//    val name: String,
//    val phone: String,
//    val speciality: String,
//    val priority: String,
//    val score: String,
//    val snils: String,
//    val comment: String
//)

data class EntrantResponse(val data: List<Entrant>)

data class Entrant(
    val attributes: Attributes
)

data class Attributes(
    val name: String,
    val phone: String,
    val comment: String,
    val score: String,
    val priority: String,
    val specialty: String,
    val snils: String
)