package com.example.practice.network

import com.example.practice.Entrant
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class ApiClient{
    companion object{
        private val retrofit: Retrofit by lazy{
            with(Retrofit.Builder()){
                baseUrl("http://51.250.45.188/crm/suite/Api/V8/")
                addConverterFactory(GsonConverterFactory.create())
                build()
            }
        }
        val entrantReq: EntrantAPI by lazy{
            retrofit.create(EntrantAPI::class.java)
        }
    }
}