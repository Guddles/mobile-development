package com.example.practice.network

import com.example.practice.Entrant
import com.example.practice.EntrantResponse
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header

interface EntrantAPI {

    @GET("module/demo1_EntrantModule")
//    suspend fun getEntrants(@Header("Authorization") authToken: String): Call<Entrant>
    suspend fun getEntrants(@Header("Authorization") authToken: String): Response<EntrantResponse>
}