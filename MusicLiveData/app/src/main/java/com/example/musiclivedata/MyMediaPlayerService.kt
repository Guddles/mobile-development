package com.example.musiclivedata

import android.app.Service
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData


class MyMediaPlayerService(): Service() {


    private var mediaPlayer = MediaPlayer()

    var IS_NEW_TRACK = true

    private val handler = Handler(Looper.getMainLooper())

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        TRACK_NAME.observeForever() {
            setTrack()
        }

        isPlaying.observeForever() {
            when(it) {
                true ->
                    start()
                false ->
                    pause()
            }
        }

        return START_STICKY
    }

    override fun onBind(intent: Intent): IBinder? = null


    fun setTrack() {
        IS_NEW_TRACK = true

        if (isPlaying.value == true) {
            pause()

            start()
        }
    }

    fun start() {

        if (!IS_NEW_TRACK) {
            mediaPlayer.start()
        } else {
            val afd = assets.openFd(TRACK_NAME.value!!)
            mediaPlayer = MediaPlayer()
            mediaPlayer.setDataSource(afd.fileDescriptor, afd.startOffset, afd.length)

            IS_NEW_TRACK = false

            afd.close()
            mediaPlayer.prepare()
            mediaPlayer.start()
        }

        handler.postDelayed(updateDataTrackSheduler, 1000L)
    }

    fun pause() {
        mediaPlayer.pause()

        handler.removeCallbacks(updateDataTrackSheduler)
    }

    private val updateDataTrackSheduler = Runnable {


        if (mediaPlayer.isPlaying) {
            updateDataTrack()
        }
    }

    private fun updateDataTrack() {


        liveDataDuration.value = mediaPlayer.duration
        liveDataNowPosition.value = mediaPlayer.currentPosition

        handler.postDelayed(updateDataTrackSheduler, 1000)
    }

    companion object {

        val TRACK_NAME = MutableLiveData<String>()
        val isPlaying = MutableLiveData<Boolean>()

        private val liveDataDuration = MutableLiveData(0)
        private val liveDataNowPosition = MutableLiveData(0)


        fun getLiveDataDuration(): LiveData<Int> {
            return liveDataDuration
        }
        fun getLiveDataNowPosition(): LiveData<Int> {
            return liveDataNowPosition
        }

        fun startService(context: Context) {
            context.startService(Intent(context, MyMediaPlayerService::class.java))
        }

        fun stopService(context: Context) {
            context.stopService(Intent(context, MyMediaPlayerService::class.java))
        }
    }
}