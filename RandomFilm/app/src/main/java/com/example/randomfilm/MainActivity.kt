package com.example.randomfilm

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import java.util.*

class MainActivity : AppCompatActivity() {
    private lateinit var films: ArrayList<String>
    private lateinit var textView: TextView
    private lateinit var FilmButtonNew: Button
    private lateinit var rand: Random
    private lateinit var resB: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        films = resources.getStringArray(R.array.films).toCollection(ArrayList())
        textView = findViewById(R.id.textView)
        FilmButtonNew = findViewById(R.id.button1)
        resB = findViewById(R.id.button2)
        rand = Random()
    }
    fun FilmClick(view: View) {
        if (view.id == R.id.button1) {
            if (films.size == 0) {
                textView.text = "Фильмов больше нет"
            } else {
                val idx = rand.nextInt(films.size)
                textView.text = films[idx]
                films.removeAt(idx)
            }
        }
        else if (view.id == R.id.button2){
            textView.text = "Нажмите кнопку"
            films = resources.getStringArray(R.array.films).toCollection(ArrayList())
        }
    }
}