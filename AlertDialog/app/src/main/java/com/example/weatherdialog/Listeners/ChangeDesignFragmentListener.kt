package com.example.weatherdialog.Listeners

import android.content.DialogInterface
import androidx.fragment.app.FragmentManager
import com.example.weatherdialog.R
import com.example.weatherdialog.fragments.FragmentWeatherBriefly
import com.example.weatherdialog.fragments.FragmentWeatherDetail

class ChangeDesignFragmentListener(private val fragmentManager: FragmentManager): DialogInterface.OnClickListener {

    override fun onClick(dialog: DialogInterface?, which: Int) {
        when (which) {
            0 -> fragmentManager.beginTransaction()
                .replace(R.id.main_fragment_layout, FragmentWeatherDetail.newInstance())
                .commit()
            1 -> fragmentManager.beginTransaction()
                .replace(R.id.main_fragment_layout, FragmentWeatherBriefly.newInstance())
                .commit()
        }
    }

}