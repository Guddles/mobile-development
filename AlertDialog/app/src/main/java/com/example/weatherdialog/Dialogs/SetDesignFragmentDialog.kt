package com.example.weatherdialog.Dialogs

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.example.weatherdialog.Listeners.ChangeDesignFragmentListener
import com.example.weatherdialog.R

class SetDesignFragmentDialog() : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        return activity?.let {
            val builder = AlertDialog.Builder(it)

            builder.setTitle("Change type fragment").setItems(context?.resources?.getStringArray(R.array.fragmentTypeDesign), ChangeDesignFragmentListener(parentFragmentManager))
                builder.create()
            } ?: throw IllegalStateException("Activity cannot be null")
    }
}