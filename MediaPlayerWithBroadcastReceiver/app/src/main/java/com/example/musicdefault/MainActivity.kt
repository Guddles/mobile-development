package com.example.musicdefault

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.musicdefault.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    var isPlaying = false

    override fun onCreate(savedInstanceState: Bundle?) {


        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        MediaPlayerService.startService(applicationContext, TRACK_NAME)

        binding.playOrStop.setOnClickListener(playOrStopButtonListener)

        binding.toSecondActivity.setOnClickListener(toSecondActivityButtonListener)
    }

    override fun onResume() {
        super.onResume()


        isPlaying = IS_PLAYING ?: false
        IS_PLAYING = null
        updatePlayOrStopButton()
    }

    private val playOrStopButtonListener = { view: View ->
        sendBroadcast(Intent().apply {
            action = CONSTANTS.ID.value
            putExtra(CONSTANTS.STRING_DATA_TYPE_KEY.value, CONSTANTS.SWITCH_STATE.value)
        })

        isPlaying = !isPlaying
        updatePlayOrStopButton()
    }

    private val toSecondActivityButtonListener = { view: View ->
        startActivity(Intent(this, DetailActivity::class.java).apply {
            putExtra(CONSTANTS.STRING_DATA_IS_PLAYING_KEY.value, isPlaying)
        })
    }

    private fun updatePlayOrStopButton() {
        if (isPlaying)
            binding.playOrStop.setImageResource(R.drawable.pause)
        else
            binding.playOrStop.setImageResource(R.drawable.play)
    }

    override fun onDestroy() {
        super.onDestroy()

        MediaPlayerService.stopService(applicationContext)
    }

    companion object {

        var IS_PLAYING: Boolean? = null

        const val TRACK_NAME = "wildfire.mp3"
    }
}