package com.example.recycle

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    val planetsList = arrayListOf<String>("Mars", "Venus", "Earth")
    // TODO: реализовать генерацию цветов определённой палитры
//    val colorsList = mutableListOf(Color.YELLOW, Color.RED, Color.GREEN, Color.MAGENTA)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val listOfColors = mutableListOf(
            ResourcesCompat.getColor(resources, R.color.color_1, null),
            ResourcesCompat.getColor(resources, R.color.color_2, null),
            ResourcesCompat.getColor(resources, R.color.color_3, null),
            ResourcesCompat.getColor(resources, R.color.color_4, null),
            ResourcesCompat.getColor(resources, R.color.color_5, null),
            ResourcesCompat.getColor(resources, R.color.color_6, null),
            ResourcesCompat.getColor(resources, R.color.color_7, null)

        )
        // пример использования ListView
//        val lv = findViewById<ListView>(R.id.list)
//        val adapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, planetsList)
//        lv.adapter = adapter

        // пример использования RecyclerView с собственным адаптером
        val rv = findViewById<RecyclerView>(R.id.rview)
        val colorAdapter = ColorAdapter(LayoutInflater.from(this))
        // добавляем данные в список для отображения
        colorAdapter.submitList(listOfColors)
        rv.layoutManager = LinearLayoutManager(this)
        rv.adapter = colorAdapter

    }
}