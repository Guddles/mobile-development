package com.example.listviewcool

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ListView
import org.json.JSONArray
import org.json.JSONObject

class MainActivity : AppCompatActivity() {
    private lateinit var adapter: UserListAdapter
    private lateinit var listView: ListView
    private val users = ArrayList<User>()

    private fun getJson(context: Context, file: String): String?{
        val jsString: String
        jsString = context.assets.open(file).bufferedReader().use{it.readText()}
        return jsString
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        listView = findViewById(R.id.list)
        val jsString = getJson(this, "info.json")
        val info = JSONObject(jsString.toString()).getJSONArray("info") as JSONArray
        for (i in 0 until info.length()){
            users.add(User(info.getJSONObject(i).getString("name"),
                            info.getJSONObject(i).getString("phone"),
                            info.getJSONObject(i).getString("sex")))
        }
        adapter = UserListAdapter(this, users)
        listView.adapter = adapter
    }
    fun sort(view: View){
        when(view.id){
            R.id.sort_name ->{
                users.sortWith(compareBy{it.name})
            }
            R.id.sort_sex ->{
                users.sortWith(compareBy{it.sex})
            }
            R.id.sort_phone -> {
                users.sortWith(compareBy{it.phoneNumber})
            }
        }
        adapter.notifyDataSetInvalidated()
    }
}