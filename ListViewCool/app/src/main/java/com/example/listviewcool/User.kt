package com.example.listviewcool

class User(var name: String, var phoneNumber: String, private val sexString: String){
    val sex = when(sexString){
        "M" -> Sex.MAN
        "F" -> Sex.WOMAN
        else -> Sex.UNKNOWN
    }
}
