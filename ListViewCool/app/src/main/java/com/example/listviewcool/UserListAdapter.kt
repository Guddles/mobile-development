package com.example.listviewcool

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import java.util.*
import kotlin.collections.ArrayList
class UserListAdapter (cont: Context, users: ArrayList<User> ):BaseAdapter() {
    var cont: Context
    var users: ArrayList<User>

    init {
        this.cont = cont
        this.users = users
    }
    override fun getCount(): Int {
        return users.size
    }

    override fun getItem(position: Int): Any {
        return users[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var convertView = convertView
//        val start = Date()
        val user_ = users[position]


        convertView = LayoutInflater.from(cont).inflate(R.layout.itemsus, parent, false)

        val userspic = convertView.findViewById<ImageView>(R.id.userpic)
        userspic.setOnClickListener{v -> v.setBackgroundColor(Color.RED)}
        val name = convertView.findViewById<TextView>(R.id.name)
        val phone = convertView.findViewById<TextView>(R.id.phone)

        name.text = user_.name
        phone.text = user_.phoneNumber

        when(user_.sex){
            Sex.MAN -> userspic.setImageResource(R.drawable.user_man)
            Sex.WOMAN -> userspic.setImageResource(R.drawable.user_woman)
            Sex.UNKNOWN -> userspic.setImageResource(R.drawable.user_unknown)
        }
        return convertView

    }
}