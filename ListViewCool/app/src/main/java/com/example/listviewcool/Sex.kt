package com.example.listviewcool

enum class Sex {
    MAN, WOMAN, UNKNOWN
}
