package com.example.tabsdemo

import org.json.JSONObject
import java.io.IOException
import java.io.InputStream
import java.net.URL
import java.util.Scanner

class getWeather {
    fun weatherGet(city: String, api: String): Weather{
        lateinit var stream:InputStream
        val url_req = "https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${api}&units=metric"
        var data = " "
        val weather = Weather()

        try {
            stream = URL(url_req).content as InputStream
        } catch (e: IOException) {
            weather.name = "Error Internet connection!"
        }

        try {
            data = Scanner(stream).nextLine() ?: ""

            val jsonObj = JSONObject(data)

            weather.temp = jsonObj.getJSONObject("main").getString("temp").let { "$it°" }
        } catch (e: Exception) {
            weather.name = "Ошибка запроса! (проверьте название города)"
        }

        return weather
    }
}