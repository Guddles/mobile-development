package com.example.tabsdemo

import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import androidx.viewpager.widget.ViewPager
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.example.tabsdemo.ui.main.SectionsPagerAdapter
import com.example.tabsdemo.databinding.ActivityMainBinding
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // data binding предусмотрен изначально в заготовке проекта
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

    }

    override fun onResume() {
        super.onResume()

        val cities = resources.getStringArray(R.array.cities)
        val api =resources.getString(R.string.API_KEY)

        var weatherData = mutableListOf<Weather>()

        for (city in cities){
            weatherData.add(getWeather().weatherGet(city, api))
        }

        // список "табов"
        val sectionsPagerAdapter = SectionsPagerAdapter(this, ArrayList(weatherData), supportFragmentManager)
        val viewPager: ViewPager = binding.viewPager
        viewPager.adapter = sectionsPagerAdapter

        val tabs: TabLayout = binding.tabs
        tabs.setupWithViewPager(viewPager)
    }
}